#pragma once
#include <stdint.h>
#include <functional>
//ͳһstack����
class TcpipStack
{
public:
	using TxHandlerType = std::function<bool(const uint8_t*, size_t)>;
	using TxHandlerTypeEx = std::function<bool(const uint32_t, const uint32_t, const uint8_t*, size_t)>;
	TcpipStack() { _TxHandler = nullptr; _TxHandlerEx = nullptr; };
	~TcpipStack() {};
protected:
	TxHandlerType _TxHandler;
	TxHandlerTypeEx _TxHandlerEx;
public:
	virtual bool ProcessRx(const uint8_t *packet, size_t packet_len) = 0;
	virtual bool ProcessRxEx(const uint32_t dstIp, const uint32_t srcIp, const uint8_t *packet, size_t packet_len) = 0;
	virtual bool TxProcess(const uint8_t *packet, size_t packet_len) = 0;
	void RegisterTxHandler(TxHandlerType _hanler) {
		_TxHandler = _hanler;
	};
	void RegisterTxHandlerEx(TxHandlerTypeEx _HandlerEx) {
		_TxHandlerEx = _HandlerEx;
	}
};