#include "stdafx.h"
#include <IPHlpApi.h>

CArp arp;

CArp::CArp()
{
	//这里弄
	init_win_nic_info();
}


CArp::~CArp()
{
	_nic_info.clear();
}

bool CArp::ProcessRx(const uint8_t *packet, size_t packet_len)
{
	bool bret = false;
	auto arphdr = reinterpret_cast<arp_header*>((void *)packet);
	//LOG_DEBUG("ArpOp=%x\r\n", arphdr->arp_op);
	switch (arphdr->arp_op)
	{
	case 0x0200:
		//LOG_DEBUG("reply\r\n");
		//dumpMac(arphdr->arp_sha);
		//dumpMac(arphdr->arp_tha);
		//dumpIP((uint8_t *)&arphdr->arp_spa);
		//dumpIP((uint8_t *)&arphdr->arp_tpa);
		arp_add_ip_mac(arphdr->arp_spa, arphdr->arp_sha);
		bret = true;
		break;
	case 0x0100:
		//LOG_DEBUG("request\r\n");
		//arp_add_ip_mac(arphdr->arp_spa, arphdr->arp_sha);
		//dumpMac(arphdr->arp_sha);
		//dumpIP((uint8_t *)&arphdr->arp_spa);
		//dumpIP((uint8_t *)&arphdr->arp_tpa);
		//做个demo的ip 192.168.1.105
		bret =  ReplyArp(packet, packet_len);
		//todo::结合IP的本地ip dhcp重新分配
		break;
	default:
		break;
	}
	return bret;
}

bool CArp::TxProcess(const uint8_t *packet, size_t packet_len)
{
	if (!_TxHandler)
		return false;
	return _TxHandler(packet, packet_len);
}

const uint8_t* CArp::Protocol2Hardware(const uint8_t* protocolAddress)
{
	auto _ip = *(ULONG*)protocolAddress;
	auto ip = arp_ip_from_ip(_ip);
	auto bis_zero = is_zero_ip((uint8_t *)Ipv4.getLocalIp());
	auto myip = *(ULONG*)Ipv4.getLocalIp();
	auto ptr = m_mac_map.find(ip);
	if (ptr != m_mac_map.end())
	{
		return ptr->second.mac;
	}

	/* 发送mac请求 */
	for (auto nic_info = _nic_info.cbegin(); nic_info != _nic_info.cend(); nic_info++)
		if ((nic_info->ip & nic_info->mask) == (ip & nic_info->mask))
			SendArpQuary(ip, bis_zero?nic_info->ip:myip, (void *)(bis_zero ? nic_info->mac : Eth.getMac()));

	return NULL;
}
bool CArp::ReplyArp(const uint8_t*packet, size_t packet_len)
{
	auto ipv4ip = Ipv4.getLocalIp();
	if (is_zero_ip((uint8_t *)ipv4ip))
	{
		//全局模式
		return false;
	}
	//有IP那么检查是不是查我并回复
	unsigned char arp_pack[sizeof(struct arp_header)] = {};
	auto arp_hdr = (struct arp_header *)(arp_pack);
	auto arp_hdr_src = (struct arp_header *)((void*)packet);
	if (!Ipv4.isLocalIp((const uint8_t*)&arp_hdr_src->arp_tpa))
		return false;
	init_arp_header(arp_hdr, arp_hdr_src->arp_spa, arp_hdr_src->arp_sha, arp_hdr_src->arp_tpa, (void*)m_mac_map[arp_hdr_src->arp_tpa].mac);
	arp_hdr->arp_op = 0x0200;
	return Eth.TxPacket(arp_hdr_src->arp_sha,ETH_PROTO_ARP,arp_pack, sizeof(arp_pack));
}
/*
* 发送arp查询报文-网络字节次序
*/
bool CArp::SendArpQuary(unsigned long dst_ip, unsigned long src_ip, void* src_mac)
{
	unsigned char arp_pack[sizeof(struct arp_header)];
	struct arp_header* arp_hdr;

	/* 填写arp头信息 */
	arp_hdr = (struct arp_header*)(arp_pack);
	init_arp_header(arp_hdr, dst_ip, NULL, src_ip, (void*)src_mac);
	arp_hdr->arp_op = 0x0100;

	return TxProcess(arp_pack, sizeof(arp_pack));
}

void CArp::init_arp_header(struct arp_header* arp_hdr, unsigned long dst_ip, void* dst_mac, unsigned long src_ip, void* src_mac)
{
	if (arp_hdr == NULL)
		return;

	memset(arp_hdr, 0, sizeof(struct arp_header));

	arp_hdr->arp_hln = 0x06;
	arp_hdr->arp_hrd = ETH_PROTO;
	arp_hdr->arp_pln = 0x04;
	arp_hdr->arp_pro = ETH_PROTO_IP;

	arp_hdr->arp_spa = src_ip;
	arp_hdr->arp_tpa = dst_ip;

	if (src_mac)
		memcpy(arp_hdr->arp_sha, src_mac, 6);

	if (dst_mac)
		memcpy(arp_hdr->arp_tha, dst_mac, 6);
}


/*
* 添加一个ip<->mac对应关系
* 注意ip为网络字节序
*/
int CArp::arp_add_ip_mac(unsigned long ip, void *mac)
{
	if (ip == 0 || mac == NULL)
		return -1;

	ip = arp_ip_from_ip(ip);

	mac_store macx = {};
	RtlCopyMemory(macx.mac, mac, 6);
	m_mac_map[ip] = macx;

	return 0;
}

unsigned long CArp::arp_ip_from_ip(unsigned long ip)
{
	unsigned long arp_ip = 0;
	/* 将ip 转换为内网地址 - arp协议只支持内网地址 */
	for (auto nic_info = _nic_info.cbegin(); nic_info!=_nic_info.cend(); nic_info++)
	{
		////LOG_DEBUG("dump nicinfo begin\r\n");
		//dumpIP((uint8_t *)&nic_info->ip);
		//dumpIP((uint8_t *)&nic_info->gate_ip);
		//dumpMac((uint8_t *)nic_info->mac);
		//dumpIP((uint8_t*)&nic_info->mask);
		//LOG_DEBUG("dump nicinfo end\r\n");
		if ((nic_info->ip & nic_info->mask) == (ip & nic_info->mask)) {
			arp_ip = ip;
			break;
		}
		else if (nic_info->gate_ip != 0) {
			arp_ip = nic_info->gate_ip;
		}
	}
	//LOG_DEBUG("find arpip=");
	//dumpIP((uint8_t *)&arp_ip);
	return arp_ip;
}

void CArp::init_win_nic_info()
{
	ULONG adapter_info_size;
	PIP_ADAPTER_INFO adapter_info;
	sock_net_nic_info win_nic_info_enum = {};

	_nic_info.clear();
	adapter_info_size = 0;
	adapter_info = NULL;

	/* 获取缓冲大小,申请初始化win_nic_info内存 */
	if (GetAdaptersInfo(adapter_info, &adapter_info_size) == ERROR_BUFFER_OVERFLOW) {
		adapter_info = (IP_ADAPTER_INFO*)malloc(adapter_info_size);
	}

	if (GetAdaptersInfo(adapter_info, &adapter_info_size) != ERROR_SUCCESS) {
		free(adapter_info);
		return;
	}

	/* 遍历所有适配器链表 */
	for (; adapter_info != NULL; adapter_info = adapter_info->Next)
	{
		/*
		* 判断是否为以太网接口
		* adapter_info->Description 是适配器描述
		* adapter_info->AdapterName 是适配器名称
		* adapter_info->Address     是Mac地址
		*
		*/
		if (adapter_info->Type == MIB_IF_TYPE_ETHERNET) {
			/* inet_addr失败返回INADDR_NONE */
			win_nic_info_enum.ip = inet_addr(adapter_info->IpAddressList.IpAddress.String);
			if (win_nic_info_enum.ip == INADDR_NONE)
				win_nic_info_enum.ip = 0;

			win_nic_info_enum.gate_ip = inet_addr(adapter_info->GatewayList.IpAddress.String);
			if (win_nic_info_enum.gate_ip == INADDR_NONE)
				win_nic_info_enum.gate_ip = 0;

			if (win_nic_info_enum.ip == 0 || win_nic_info_enum.gate_ip == 0)
				continue;

			win_nic_info_enum.mask = inet_addr(adapter_info->IpAddressList.IpMask.String);
			memcpy(win_nic_info_enum.mac, adapter_info->Address, 6);

			//向ip和Eth添加地址
			Eth.addMac(win_nic_info_enum.mac);
			Ipv4.addIp(win_nic_info_enum.ip);
			mac_store _local = {};
			RtlCopyMemory(_local.mac, win_nic_info_enum.mac, 6);
			m_mac_map[win_nic_info_enum.ip] = _local;
			_nic_info.push_back(win_nic_info_enum);
		}
	}
	free(adapter_info);
}
