#include "stdafx.h"


CSocketTcp::CSocketTcp()
{
	ReadByte = 0;
	local_port = 0;
	lastError = SUCCESS_TCP;
	TcpInternal.regTcpClient(this);
}


CSocketTcp::~CSocketTcp()
{
	_RxLock.lock();
	RxBuffer.clear();
	_RxByteBuffer.clear();
	_RxByteBuffer.resize(0);
	_RxLock.unlock();
}

int16_t CSocketTcp::getLastError() {
	return lastError;
}
void CSocketTcp::setLastError(int16_t error_code)
{
	lastError = error_code;
}
bool CSocketTcp::TcpBind(int nport) {
	local_port = sock_htons(nport);
	return TcpInternal.BindPort(local_port);
}
bool CSocketTcp::TcpConnect(uint32_t ipaddr, int nport) {
	return TcpInternal.Connect(sock_htons(nport), ipaddr);
}
bool CSocketTcp::TcpLisent() {
	return TcpInternal.Listen();
}
//////////////////////////////////////////////////////////////////////////
CSocketTcp*CSocketTcp::Accept() {
	//这个就是全新的世界了
	while (_newList.empty()) { Sleep(0); };
	auto ret = _newList.front();
	_newList.pop();
	ret->WaitOK();
	return ret;
}
void CSocketTcp::WaitOK()
{
	TcpInternal.WaitHandleOk();
}
void CSocketTcp::Accept(uint16_t port, uint32_t ip, TCPPOOL_PACKET *packet)
{
	TcpInternal.Accept(port, ip, packet);
}
//////////////////////////////////////////////////////////////////////////
size_t CSocketTcp::blockRecv(uint8_t *buffer, size_t buffersize) {
	if (TcpInternal.state!=ESTABLISHED)
	{
		setLastError(NOT_ESTABLISHED);
		return size_t(-1);
	}
	if (RxBuffer.capacity()==0)
	{
		setLastError(RECV_NO_DATA);
		return -1;
	}
	if(RxWait.Wait(30000)==false||RxBuffer.capacity())
	{
		//30000ms 30s没有接收到数据
		//排序,从小到大
		_RxLock.lock();
		std::sort(RxBuffer.begin(), RxBuffer.end(), [](auto x1, auto x2) {
			return (x1.Seq < x2.Seq);
		});
		size_t real_recv_size = 0;
		size_t reallocSize = RxBuffer.capacity()*MAX_TCP_MSS;
		uint64_t FirstSeq = RxBuffer.begin()->Seq;
		auto preSeq = FirstSeq;
		auto preSize = RxBuffer.begin()->size;
		_RxByteBuffer.resize(reallocSize);
		for (auto &Rx : RxBuffer)
		{
			//LOG_DEBUG("seq=%u %u\r\n", Rx.Seq, Rx.Seq - FirstSeq);
			if (preSeq != Rx.Seq&&preSeq + preSize != Rx.Seq)
			{
				//发现缺少包
				LOG_DEBUG("缺包\r\n");
				auto reneedseq = uint32_t(preSeq&0xffffffff) + preSize;
				TcpInternal.put_reneed(reneedseq);
				break;
			}
			RtlCopyMemory(_RxByteBuffer.data() + Rx.Seq - FirstSeq, Rx.Buffer, Rx.size);
			real_recv_size += Rx.size;
			preSeq = Rx.Seq;
			preSize = Rx.size;
		}
		//LOG_DEBUG("FFF\r\n");
		auto cpySize = min(buffersize, real_recv_size-ReadByte);
		RtlCopyMemory(buffer, _RxByteBuffer.data()+ReadByte, cpySize);
		ReadByte += cpySize;
		TcpInternal.resizeWnd(cpySize);
		if (ReadByte == _RxByteBuffer.capacity())
		{
			RxBuffer.clear();
			RxBuffer.resize(0);
			ReadByte = 0;
			RxWait.Reset();
		}
		_RxByteBuffer.clear();
		_RxByteBuffer.resize(0);
		_RxLock.unlock();
		setLastError(SUCCESS_TCP);
		return cpySize;
	}
	setLastError(RECV_ERROR);
	return size_t(-1);
}
void CSocketTcp::Clear(){
	//TcpInternal断开连接走起
	TcpInternal.Close();
}

void CSocketTcp::pushRxData(uint8_t *data, uint32_t index, uint16_t _size)
{
	LOG_DEBUG("%s\r\n", (char*)data);
	//LOG_DEBUG("index=%u size=%d\r\n", index, _size);
	RX_BUFFER buf = {};
	//计算真正的序号，防止歇逼，这里有个问题,就是在OVER之后可能收到OVER前的包
	//所以还需要分离机制
	buf.Seq = TcpInternal.OverCount * 0x100000000ui64 + index;
	buf.size = _size;
	RtlCopyMemory(buf.Buffer, data, _size);
	_RxLock.lock();
	auto ptr = std::find_if(RxBuffer.begin(), RxBuffer.end(), [&](auto rx) {
		return rx.Seq == buf.Seq;
	});
	if(ptr==RxBuffer.end())
		RxBuffer.push_back(buf);
	_RxLock.unlock();
	RxWait.Set();
	return;
}

void CSocketTcp::pushRxPointer()
{

}


size_t CSocketTcp::blockSend(const uint8_t *buffer, size_t buffersize)
{
	if (TcpInternal.state != ESTABLISHED)
	{
		setLastError(NOT_ESTABLISHED);
		return size_t(-1);
	}
	if (buffersize > MAX_TCP_BUFFER)
	{
		size_t sent_size = 0;
		for (size_t i=0;i<buffersize;i+=MAX_TCP_BUFFER-1)
		{
			if (TcpInternal.TxPacket((uint8_t*)&buffer[i], MAX_TCP_BUFFER-1)
				!= MAX_TCP_BUFFER-1)
			{
				return i;
			}
			else{
				sent_size = i;
			}
		}
		auto remind = buffersize - sent_size;
		if (remind)
		{
			sent_size += TcpInternal.TxPacket((uint8_t*)&buffer[sent_size], (uint16_t)remind);
		}
		return sent_size;
	}
	else {
		return TcpInternal.TxPacket((uint8_t*)buffer, (uint16_t)buffersize);
	}
}