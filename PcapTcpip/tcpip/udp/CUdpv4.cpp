#include "stdafx.h"

CUdpv4 udpv4;

CUdpv4::CUdpv4()
{
}


CUdpv4::~CUdpv4()
{
}
//udp的池:
//过滤bind PORT 
//数据写入数据RxBuffer[RxOffset]
//SetEventRx
//RecvFrom里是WaitForEventRx
//Sendto

bool CUdpv4::ProcessRxEx(const uint32_t dstIp, const uint32_t srcIp, const uint8_t *packet, size_t packet_len)
{
	bool bret = false;
	LOG_DEBUG("%s\r\n",__FUNCDNAME__);
	//开始处理
	IPINFO ipinfo = {};
	ipinfo.local_ip = dstIp;
	ipinfo.remote_ip = srcIp;
	auto buffer = (uint8_t*)packet;
	auto newlen = packet_len - 8;
	auto newbuf = buffer + 8;

	auto udphdr = reinterpret_cast<udp_header*>(buffer);
	auto len = sock_htons(udphdr->uh_ulen);

	ipinfo.remote_port = udphdr->uh_sport;
	ipinfo.local_port = udphdr->uh_dport;

	bret = CSocketUDPPool::getInstance().ProcessRx(&ipinfo, newbuf, newlen);

	return bret;
}

bool CUdpv4::TxPacket(const uint32_t dstIp, const uint32_t srcIp, const uint16_t dst_port, const uint16_t src_port, const uint8_t *packet, size_t packet_len)
{
	bool bret = false;
	if (packet_len > MAX_UDP_PACKET_SIZE)
	{
		LOG_DEBUG("too long udp\r\n");
		return false;
	}
	//udp发包，构造包头，发送
	uint8_t udp_pack[MAX_UDP_PACKET_SIZE * 2] = {};
	RtlZeroMemory(udp_pack, sizeof(udp_pack));
	//udp头，udp伪头
	//算checksum
	auto udphdr = reinterpret_cast<udp_header*>(udp_pack);
	auto psd_udphdr = reinterpret_cast<udp_psd_header*>(udp_pack + 8);
	psd_udphdr->dst_addr = dstIp;
	psd_udphdr->src_addr = srcIp;
	psd_udphdr->proto = IP_PROTO_UDP;
	psd_udphdr->zero = 0;
	psd_udphdr->udp_len = sock_htons(8 + (uint16_t)packet_len);
	udphdr->uh_dport = dst_port;
	udphdr->uh_sport = src_port;
	udphdr->uh_sum = 0;
	udphdr->uh_ulen = sock_htons(8 + (uint16_t)packet_len);
	auto sum = rfc_checksum((void*)udphdr, 20, (void *)packet, (int)packet_len);
	udphdr->uh_sum = sum;
	RtlCopyMemory(udp_pack + 8, packet, packet_len);
	if(!_TxHandlerEx)
		return Ipv4.TxPacket(dstIp, srcIp,IP_PROTO_UDP, udp_pack, packet_len + 8);
	return _TxHandlerEx(dstIp, srcIp, udp_pack, packet_len + 8);
}
