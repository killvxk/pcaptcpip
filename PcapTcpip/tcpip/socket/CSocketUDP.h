#pragma once
struct _RXDATA_
{
	uint8_t RxBuffer[MAX_RX_BUFFER_SIZE];
	IPINFO curIpInfo;
	uint16_t RxOffset;
	uint16_t RecvOffset;
};
class CSocketUDP
{
public:
	CSocketUDP();
	~CSocketUDP();
private:
	osEvent RxEvent;
	uint16_t port;
	spinlock_mutex _RxLock;
	std::queue<_RXDATA_>_RxData;
public:
	bool bindport(int nport);
	size_t recvfrom(IPINFO *ipinfo, const uint8_t*buffer, size_t wanna_size);
	bool sendto(IPINFO *dstipinfo, const uint8_t*buffer, size_t _length);
public:
	bool RxData(IPINFO *ip, uint8_t *buff, size_t _length);
};

