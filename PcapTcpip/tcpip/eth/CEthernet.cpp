#include "stdafx.h"
#include "CEthernet.h"

CEthernet Eth;

CEthernet::CEthernet()
{
	RtlZeroMemory(hostMac, sizeof(hostMac));
	BroadcastAddress[0] = 0xFF;
	BroadcastAddress[1] = 0xFF;
	BroadcastAddress[2] = 0xFF;
	BroadcastAddress[3] = 0xFF;
	BroadcastAddress[4] = 0xFF;
	BroadcastAddress[5] = 0xFF;
	arp.RegisterTxHandler(
		std::bind(&CEthernet::TxPacket, this, BroadcastAddress,ETH_PROTO_ARP, std::placeholders::_1,
			std::placeholders::_2));

	auto thread = std::thread(std::bind(&CEthernet::RxThread, this));
	thread.detach();
}


CEthernet::~CEthernet()
{
}

bool CEthernet::PspRx(const uint8_t *packet, size_t packet_len)
{
	bool ret = false;
	auto buffer = (uint8_t *)(packet);
	auto eth = reinterpret_cast<eth_header *>(buffer);
	auto newbuf = buffer + ETH_HEADER_LENGTH;
	auto new_len = packet_len - ETH_HEADER_LENGTH;
	auto ipv4ip = Ipv4.getLocalIp();
	auto isnoip = is_zero_ip((uint8_t *)ipv4ip);
	//dumpMac(eth->dst_mac);
	auto isBCA =  is_same_mac_address(BroadcastAddress, eth->dst_mac);
	if (isLocalMac(eth->dst_mac)||isBCA)
	{
		//LOG_DEBUG("MyPacket %x\r\n", eth->type);
		//发给我们的，分析协议
		switch (eth->type)
		{
		case ETH_PROTO_ARP:
			//LOG_DEBUG("ARP\r\n");
			ret = arp.ProcessRx(newbuf,new_len);
			break;
		case ETH_PROTO_IP:
			//LOG_DEBUG("IP\r\n");
			if (isnoip && isBCA)
			{
				ret = Ipv4.ProcessRx(newbuf, new_len);
			}
			else if(!isBCA&&!isnoip)
			{
				ret = Ipv4.ProcessRx(newbuf, new_len);
			}
			break;
		case ETH_PROTO_IPV6:
			//ipv6
			break;
		default:
			LOG_DEBUG("unknown %x\r\n",eth->type);
			break;
		}
	}
	return ret;
}

bool CEthernet::TxProcess(const uint8_t *packet, size_t packet_len)
{
	//Eth是最后一层了，直接发包
	return _TxHandler(packet, packet_len);
}

void CEthernet::setHostMac(uint8_t *_hostMac)
{
	dumpMac(_hostMac);
	RtlCopyMemory(hostMac, _hostMac, 6);
}


bool CEthernet::TxPacket(uint8_t *_targetMac,uint16_t Type,const uint8_t *packet, size_t packet_len)
{
	eth_header header = {};
	RtlCopyMemory(header.src_mac, hostMac, 6);
	RtlCopyMemory(header.dst_mac, _targetMac, 6);
	header.type = Type;
	//构造新的包
	auto newpacket = (uint8_t *)malloc(packet_len + sizeof(eth_header));
	if (!newpacket)
	{
		LOG_DEBUG("no buff for newpacket\r\n");
		return false;
	}
	RtlCopyMemory(newpacket, &header, sizeof(eth_header));
	RtlCopyMemory(newpacket + sizeof(eth_header), packet, packet_len);
	auto bret = TxProcess(newpacket,packet_len+sizeof(eth_header));
	free(newpacket);
	return bret;
}

void CEthernet::RxThread()
{
	while (RxEvent.Wait())
	{
		while (!RxQueue.empty())
		{
			_Ethlock.lock();
			auto myPacket = RxQueue.front();
			RxQueue.pop();
			_Ethlock.unlock();
			PspRx(myPacket->packet, myPacket->size);
			free(myPacket);
		}
		RxEvent.Reset();
	}
}

bool CEthernet::ProcessRx(const uint8_t *packet, size_t packet_len)
{
	if (packet_len > MAX_RX_BUFFER_SIZE)
	{
		LOG_DEBUG("RxSize > 1600 %lld must do in rx\r\n", packet_len);
		return PspRx(packet,packet_len);
	}
	auto new_packet = (ETHPACKET*)malloc(sizeof(ETHPACKET));
	if (!new_packet)
	{
		LOG_DEBUG("copy failed\r\n");
		return PspRx(packet,packet_len);
	}
	new_packet->size = packet_len;
	RtlCopyMemory(new_packet->packet, packet, packet_len);
	_Ethlock.lock();
	RxQueue.push(new_packet);
	_Ethlock.unlock();
	RxEvent.Set();
	return true;
}