#pragma once
class CEthernet:public TcpipStack
{
public:
	CEthernet();
	~CEthernet();
private:
	//ARP
	//IP
	std::queue<ETHPACKET*> RxQueue;
	osEvent RxEvent;
	spinlock_mutex _Ethlock;
private:
	uint8_t BroadcastAddress[6];
	uint8_t hostMac[6];
	std::vector<mac_store> localMacList;
private:
	void RxThread();
	bool PspRx(const uint8_t *packet, size_t packet_len);
public:
	bool ProcessRx(const uint8_t *packet, size_t packet_len);
	void setHostMac(uint8_t *_hostMac);
	bool isLocalMac(uint8_t *_mac)
	{
		if (is_same_mac_address(hostMac, _mac))
			return true;
		for (auto _mm : localMacList)
		{
			if (is_same_mac_address(_mm.mac, _mac))
				return true;
		}
		return true;
	}
	void addMac(uint8_t *_mac)
	{
		mac_store st = {};
		RtlCopyMemory(st.mac, _mac, 6);
		localMacList.push_back(st);
	}
	const uint8_t *getMac() {
		return hostMac;
	}
	bool TxProcess(const uint8_t *packet, size_t packet_len);
	bool ProcessRxEx(const uint32_t dstIp, const uint32_t srcIp, const uint8_t *packet, size_t packet_len)
	{
		return false;
	}
public:
	bool TxPacket(uint8_t *_targetMac, uint16_t Type, const uint8_t *packet, size_t packet_len);
};

extern CEthernet Eth;

