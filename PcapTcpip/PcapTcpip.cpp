// PcapTcpip.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "packetio/PacketIO.hpp"




void init_dhcpclient();
PacketIO* PIO = nullptr;


void packet_handler(u_char* param, const struct pcap_pkthdr* header, const u_char* pkt_data)
{
	//TODO::把这部分操作塞到Eth类里。不然这个看起来不舒服
	//这里需要复制一下包 然后异步去Eth.ProcessRx，不然处理时间太久了
	Eth.ProcessRx((const uint8_t*)pkt_data, header->len);
}
bool TxData(const uint8_t * data, size_t length)
{
	return PIO->TxData((void *)data, length);
}

struct NetworkConfig
{
	int interfaceNumber;
	uint8_t mac[6];
};

NetworkConfig config = {};
uint8_t my_mac[6] = { 'B', 'A', 'D', 'B', 'O', 'Y' };
uint8_t my_ip[] = { 192,168,1,102 };
void network_thread()
{
	char device[MAX_PATH] = {};
	PacketIO::GetDevice(config.interfaceNumber, device, sizeof(device));
	LOG_DEBUG("using device %s\n", device);
	PacketIO::GetMACAddress(device, config.mac);
	

	//设置本地MAC给ETH解析器
	//Eth.setHostMac(config.mac);
	//Ipv4.setLocalIp(my_ip);
	//DHCP版需要设置
	Eth.setHostMac(my_mac);
	PIO = new PacketIO(device);
	//设置TxData给ETH层
	Eth.RegisterTxHandler(TxData);

	PIO->Start(packet_handler);
}

CSocketUDP udpclient;
int main(int argc, char* argv[])
{

	config.interfaceNumber = 1;

	LOG_DEBUG("%d bit build\n", (sizeof(void*) == 4 ? 32 : 64));

	// Start at 1 to skip the file name
	for (int i = 1; i < argc; i++)
	{
		if (!strcmp(argv[i], "-devices"))
		{
			PacketIO::DisplayDevices();
			return 0;
		}
		else if (!strcmp(argv[i], "-use"))
		{
			config.interfaceNumber = atoi(argv[++i]);
		}
		else
		{
			LOG_DEBUG("unknown option '%s'\n", argv[i]);
			return -1;
		}
	}

	auto thread = std::thread(network_thread);
	thread.detach();
	bool find = false;
	//uint8_t localip[] = { 192,168,1,103 };
	//auto udp_test_thread = std::thread(TestUdp);
	//udp_test_thread.detach();

	//auto ping_test_thread = std::thread(Testping);
	//ping_test_thread.detach();

	auto tcp_test_thread = std::thread(TestTcp);
	tcp_test_thread.detach();

	while (1)
	{
		Sleep(1000);
		init_dhcpclient();
		//if (!find)
		//{
		//	//测试ARP包
		//	uint8_t ip[] = { 192,168,1,104 };
		//	auto mac = arp.Protocol2Hardware(ip);
		//	if (mac)
		//	{
		//		dumpMac((uint8_t*)mac);
		//		//Ipv4.setLocalIp(localip);
		//		find = true;
		//		IPINFO ipinfo;
		//		uint8_t buff[] = { 0x90,0x90 };
		//		ipinfo.local_ip = *(ULONG*)(localip);
		//		ipinfo.local_port = sock_htons(9999);
		//		ipinfo.remote_ip = *(ULONG*)(ip);
		//		ipinfo.remote_port = sock_htons(18000);
		//		udpclient.bindport(9999);
		//		udpclient.sendto(&ipinfo, buff, 2);
		//	}
		//}	
	}
	return 0;
}

