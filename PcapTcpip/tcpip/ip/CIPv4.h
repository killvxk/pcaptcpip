#pragma once
class CIPv4:public TcpipStack
{
public:
	CIPv4();
	~CIPv4();
private:
	AddressInfo ipv4Addr;
	std::vector<uint32_t> ipList;
	std::queue<IPRETX> reTxQueue;
	bool bReTxThreadLive;
private:
	void ReTxThread();
	void ReTxPacket(uint32_t _ip, uint8_t*ippacket, size_t _ippacket_size);
public:
	bool ProcessRxEx(const uint32_t dstIp, const uint32_t srcIp, const uint8_t *packet, size_t packet_len)
	{
		return false;
	}
	bool ProcessRx(const uint8_t *packet, size_t packet_len);
	bool TxProcess(const uint8_t *packet, size_t packet_len);
	bool isLocalIp(const uint8_t *ip);
	bool isBroadCastIp(const uint8_t *ip);
	void setLocalIp(uint8_t *_ip);
	const uint8_t*getLocalIp() {
		return ipv4Addr.Address;
	}
	void addIp(uint32_t ipAddr) {
		ipList.push_back(ipAddr);
	}
	bool TxPacket(const uint32_t dst_ip, const uint32_t src_ip, uint8_t proto, const uint8_t *packet, size_t packet_len);

};

extern CIPv4 Ipv4;
