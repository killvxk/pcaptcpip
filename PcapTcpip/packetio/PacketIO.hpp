#pragma once
#include <pcap.h>
#include <inttypes.h>

class PacketIO
{
public:
    PacketIO();
    PacketIO(const char* name);

    typedef void (*RxDataHandler)(uint8_t* data, size_t length);
    void Start(pcap_handler handler);
    void Stop();
    bool TxData(void* data, size_t length);
    static void GetDevice(int interfaceNumber, char* buffer, size_t buffer_size);
    static int GetMACAddress(const char* adapter, uint8_t* mac);
    static void DisplayDevices();
 
private:
    const char* CaptureDevice;
    pcap_t*     adhandle;
};