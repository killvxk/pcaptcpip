#pragma once
#ifdef __VS_COMPILER_
#pragma pack(push, 1)
#pragma warning(disable:4200)
#else
#endif

struct eth_header
{
	unsigned char	dst_mac[6];			/* 目的mac */
	unsigned char	src_mac[6];			/* 源mac */
	unsigned short	type;				/* 协议 */
};

/* 定义网络协议类型字段-网络字节次序 */
#define ETH_PROTO		0x0100
#define ETH_PROTO_IP		0x0008
#define ETH_PROTO_ARP		0x0608
#define ETH_PROTO_IPV6      0xdd86

#define IP_PROTO_TCP		0x06
#define IP_PROTO_UDP		0x11
#define IP_PROTO_ICMP		0x01
#define IP_PROTO_IGMP		0x02

#define ETH_HEADER_LENGTH	14

struct arp_header
{
	unsigned short	arp_hrd;			/* 硬件类型			0x0001	以太网 */
	unsigned short	arp_pro;			/* 协议类型			0x0800	IP */
	unsigned char	arp_hln;			/* 硬件地址长度			0x06	Bytes */
	unsigned char	arp_pln;			/* 协议地址长度			0x04	Bytes */
	unsigned short	arp_op;				/* ARP操作类型			0x0001	request 0x0002 replay */
	unsigned char	arp_sha[6];			/* 发送者的硬件地址 */
	unsigned long	arp_spa;			/* 发送者的协议地址 */
	unsigned char	arp_tha[6];			/* 目标的硬件地址 */
	unsigned long	arp_tpa;			/* 目标的协议地址 */
};

/* ip选项数据结构 */
struct ip_opt_header {
	unsigned char code;                                     /* 选项类型 */
	unsigned char len;                                      /* 选项长度 */
	unsigned char offset;                                   /* 选项偏移 */
	unsigned long addrs[9];                                 /* 最多9个IP地址 */
};

/* ip头数据结构 */
struct ip_header {
	unsigned char h_len : 4;                                  /* 首部长度(4bytes单位),默认5 */
	unsigned char version : 4;                                /* 版本号 ipv4 */
	unsigned char tos;                                      /* 服务类型 */
	unsigned short len;                                     /* 数据长度(bytes) */
	unsigned short ident;                                   /* 标识(一般自动累加) */
	union {
		unsigned short flags_offset;
		struct {
			unsigned char offset_5 : 5;
			unsigned char mf : 1;			/* 多段标示 */
			unsigned char df : 1;			/* 分段标示 */
			unsigned char resv : 1;			/* 保留 */
			unsigned char offset : 8;			/* 片偏移 */
		};
	};
	unsigned char ttl;                                      /* 生存时间 */
	unsigned char proto;                                    /* 协议类型 */
	unsigned short checksum;                                /* 头部校验和 */
	unsigned long src_ip;                                   /* 源ip */
	unsigned long dst_ip;                                   /* 目的ip */
	struct ip_opt_header opts[0];                           /* ip选项 */
};


/* TCP选项定义 */
#define TCP_OPT_END             0x0
#define TCP_OPT_NOP             0x1
#define TCP_OPT_MSS             0x2                             /* 最大报文段 */
#define TCP_OPT_SCALE           0x3                             /* 窗口扩大因子 */
#define TCP_OPT_SACK            0x4                             /* SACK支持 */
#define TCP_OPT_SACK_EDGE       0x5                             /* SACK边界 */
#define TCP_OPT_TIME            0x8

#define TCP_SKB_SACKED          0x1
#define TCP_SACKS_MAX           0x4                             /* 最大SACK边界数 */

/* TCP-SOCK暂时只支持TCP_OPT_MSS */
struct tcp_opt_sack_edge {
	unsigned long left_edge;
	unsigned long right_edge;
};

struct tcp_option_field {
	union {
		unsigned long opt_value;
		struct {
			unsigned char type;
			unsigned char len;
			unsigned char shift;
		}WSopt;
		struct {
			unsigned char type;
			unsigned char len;
			unsigned short mss;
		}MSSopt;
		struct {
			unsigned char type;
			unsigned char len;
		}SACKopt;
		/* 不支持TCP的SACK选项 */
	};
};

/* TCP协议头 */
struct tcp_header {
	unsigned short src_port;                                /* 源端口 */
	unsigned short dst_port;                                /* 目的端口 */
	unsigned int seq;                                       /* 32位序列号 */
	unsigned int ack;                                       /* 32位确认号 */
	union {
		unsigned short hlen_flags;
		struct {
			unsigned char len_res;                  /* 4bits len + 4bits res */
			unsigned char FLAG_FIN : 1;
			unsigned char FLAG_SYN : 1;
			unsigned char FLAG_RST : 1;
			unsigned char FLAG_PSH : 1;
			unsigned char FLAG_ACK : 1;
			unsigned char FLAG_URG : 1;
			unsigned char FLAG_ECE : 1;//新RFC3540
			unsigned char FLAG_CWR : 1;//新RFC3540
		};
	};
	unsigned short win;                                     /* tcp窗口 */
	unsigned short sum;                                     /* tcp校验和 */
	unsigned short urp;                                     /* 紧急数据偏移 */
	unsigned char opts[0];                                  /* tcp 选项 */
};

/* TCP校验伪头部 */
struct tcp_psd_header {
	unsigned long src_addr;                                 /* 源地址 */
	unsigned long dst_addr;                                 /* 目的地址 */
	unsigned char mbz;                                      /* 置空 */
	unsigned char proto;                                    /* 协议 */
	unsigned short tcp_len;                                 /* tcp总长度+头+数据 */
};

//udp头
struct udp_header {
	unsigned short	uh_sport;		/* 源端口 */
	unsigned short	uh_dport;		/* 目标端口 */
	unsigned short	uh_ulen;		/* udp总长度:头+数据 */
	unsigned short	uh_sum;			/* udp校验值 */
};
//udp伪头
struct udp_psd_header {
	unsigned long src_addr;                                 /* 源地址 */
	unsigned long dst_addr;                                 /* 目的地址 */
	unsigned char zero;//空
	unsigned char proto;//协议
	unsigned short udp_len;//udp总长，不包含伪头
};

//ICMP
struct icmp_header {
	unsigned char Type;
	unsigned char Code;
	unsigned short checksum;
	unsigned short id;
	unsigned short sqe;
};
//DHCP
// BOOTP (rfc951) message types
#define BOOTREQUEST 1
#define BOOTREPLY   2

// Possible values for flags field
#define BOOTP_UNICAST   0x0000
#define BOOTP_BROADCAST 0x8000

// Possible values for hardware type (htype) field
#define HTYPE_ETHER     1  // Ethernet 10Mbps
#define HTYPE_IEEE802   6  // IEEE 802.2 Token Ring
#define HTYPE_FDDI      8  // FDDI
// DHCP Option codes
#define DHO_PAD                  0
#define DHO_SUBNET_MASK          1
#define DHO_TIME_OFFSET          2
#define DHO_ROUTERS              3
#define DHO_TIME_SERVERS         4
#define DHO_NAME_SERVERS         5
#define DHO_DOMAIN_NAME_SERVERS  6
#define DHO_LOG_SERVERS          7
#define DHO_COOKIE_SERVERS       8
#define DHO_LPR_SERVERS          9
#define DHO_IMPRESS_SERVERS     10
#define DHO_RESOURCE_LOCATION_SERVERS   11
#define DHO_HOST_NAME           12
#define DHO_BOOT_SIZE           13
#define DHO_MERIT_DUMP          14
#define DHO_DOMAIN_NAME         15
#define DHO_SWAP_SERVER         16
#define DHO_ROOT_PATH           17
#define DHO_EXTENSIONS_PATH     18
#define DHO_IP_FORWARDING       19
#define DHO_NON_LOCAL_SOURCE_ROUTING 20
#define DHO_POLICY_FILTER            21
#define DHO_MAX_DGRAM_REASSEMBLY     22
#define DHO_DEFAULT_IP_TTL           23
#define DHO_PATH_MTU_AGING_TIMEOUT   24
#define DHO_PATH_MTU_PLATEAU_TABLE   25
#define DHO_INTERFACE_MTU            26
#define DHO_ALL_SUBNETS_LOCAL        27
#define DHO_BROADCAST_ADDRESS        28
#define DHO_PERFORM_MASK_DISCOVERY   29
#define DHO_MASK_SUPPLIER            30
#define DHO_ROUTER_DISCOVERY         31
#define DHO_ROUTER_SOLICITATION_ADDRESS 32
#define DHO_STATIC_ROUTES           33
#define DHO_TRAILER_ENCAPSULATION   34
#define DHO_ARP_CACHE_TIMEOUT       35
#define DHO_IEEE802_3_ENCAPSULATION 36
#define DHO_DEFAULT_TCP_TTL         37
#define DHO_TCP_KEEPALIVE_INTERVAL  38
#define DHO_TCP_KEEPALIVE_GARBAGE   39
#define DHO_NIS_DOMAIN              40
#define DHO_NIS_SERVERS             41
#define DHO_NTP_SERVERS             42
#define DHO_VENDOR_ENCAPSULATED_OPTIONS 43
#define DHO_NETBIOS_NAME_SERVERS    44
#define DHO_NETBIOS_DD_SERVER       45
#define DHO_NETBIOS_NODE_TYPE       46
#define DHO_NETBIOS_SCOPE           47
#define DHO_FONT_SERVERS            48
#define DHO_X_DISPLAY_MANAGER       49
#define DHO_DHCP_REQUESTED_ADDRESS  50
#define DHO_DHCP_LEASE_TIME         51
#define DHO_DHCP_OPTION_OVERLOAD    52
#define DHO_DHCP_MESSAGE_TYPE       53
#define DHO_DHCP_SERVER_IDENTIFIER  54
#define DHO_DHCP_PARAMETER_REQUEST_LIST 55
#define DHO_DHCP_MESSAGE            56
#define DHO_DHCP_MAX_MESSAGE_SIZE   57
#define DHO_DHCP_RENEWAL_TIME       58
#define DHO_DHCP_REBINDING_TIME     59
#define DHO_VENDOR_CLASS_IDENTIFIER 60
#define DHO_DHCP_CLIENT_IDENTIFIER  61
#define DHO_NWIP_DOMAIN_NAME        62
#define DHO_NWIP_SUBOPTIONS         63
#define DHO_USER_CLASS              77
#define DHO_FQDN                    81
#define DHO_DHCP_AGENT_OPTIONS      82
#define DHO_SUBNET_SELECTION       118  // RFC3011
/* The DHO_AUTHENTICATE option is not a standard yet, so I've
allocated an option out of the "local" option space for it on a
temporary basis.  Once an option code number is assigned, I will
immediately and shamelessly break this, so don't count on it
continuing to work. */
#define DHO_AUTHENTICATE           210
#define DHO_END                    255

// DHCP message types
#define DHCPDISCOVER 1
#define DHCPOFFER    2
#define DHCPREQUEST  3
#define DHCPDECLINE  4
#define DHCPACK      5
#define DHCPNAK      6
#define DHCPRELEASE  7
#define DHCPINFORM   8

// Relay Agent Information option subtypes
#define RAI_CIRCUIT_ID  1
#define RAI_REMOTE_ID   2
#define RAI_AGENT_ID    3

// FQDN suboptions
#define FQDN_NO_CLIENT_UPDATE 1
#define FQDN_SERVER_UPDATE    2
#define FQDN_ENCODED          3
#define FQDN_RCODE1           4
#define FQDN_RCODE2           5
#define FQDN_HOSTNAME         6
#define FQDN_DOMAINNAME       7
#define FQDN_FQDN             8
#define FQDN_SUBOPTION_COUNT  8

#define ETH_ALEN           6  // octets in one ethernet header
#define DHCP_DEST_PORT    67
#define DHCP_SOURCE_PORT  68

struct DHCP_PACKET
{
	uint8_t op;//
	uint8_t htype;//硬件类型
	uint8_t hlen;//Hardware address length
	uint8_t hop;//Hop count
	uint32_t xid;//Transaction ID
	uint16_t nsec;/*Number of seconds. 16 bits.The elapsed time in seconds since the client began an address acquisition or renewal process.*/
	uint16_t flags;//16bits
	uint32_t ciaddr;//客户端ip
	uint32_t yiaddr;//自己ip
	uint32_t siaddr;//服务器ip
	uint32_t giaddr;//路由ip
	uint8_t hard_address[16];//6字节mac，10字节补0
	uint8_t shname[64];//Server host name
	uint8_t bootfilename[128];//Boot filename
	uint32_t magic_dhcp;
	uint8_t options[4];//options长度可能很长
};

struct dhcp_option_t
{
	uint8_t code;
	uint8_t length;
	uint8_t val[0];
};


union TCPFLAG{
	unsigned short hlen_flags;
	struct {
		unsigned char len_res;                  /* 4bits len + 4bits res */
		unsigned char FLAG_FIN : 1;
		unsigned char FLAG_SYN : 1;
		unsigned char FLAG_RST : 1;
		unsigned char FLAG_PSH : 1;
		unsigned char FLAG_ACK : 1;
		unsigned char FLAG_URG : 1;
		unsigned char FLAG_ECE : 1;//新RFC3540
		unsigned char FLAG_CWR : 1;//新RFC3540
};
};
#define MAX_PROTO_HEAD 128
#ifdef __VS_COMPILER_
#pragma pack(pop)
#else
#endif

struct sock_net_nic_info
{
	unsigned char mac[6];
	unsigned long ip;
	unsigned long gate_ip;
	unsigned long mask;
};

struct IPINFO
{
	uint32_t remote_ip;
	uint32_t local_ip;
	uint16_t local_port;
	uint16_t remote_port;
};
struct IPRETX
{
	uint8_t *buffer;
	size_t lens;
	uint32_t ipAddr;
};
#define ADDRESS_SIZE 4
struct AddressInfo
{
	bool     DataValid;
	uint8_t  Address[ADDRESS_SIZE];
	uint32_t IpAddressLeaseTime;
	uint32_t RenewTime;
	uint32_t RebindTime;
	uint8_t  SubnetMask[ADDRESS_SIZE];
	uint8_t  Gateway[ADDRESS_SIZE];
	uint8_t  DomainNameServer[ADDRESS_SIZE];
	uint8_t  BroadcastAddress[ADDRESS_SIZE];
};
struct mac_store
{
	uint8_t mac[6];
};

#define INADDR_NONE             0xffffffff
#define ADDR_ANY				0x00000000
extern"C"
{
	DECLSPEC_IMPORT
		unsigned long
		WINAPI
		inet_addr(
			_In_z_ const char FAR * cp
		);
};

#define MAX_RX_BUFFER_SIZE 1600
#define MAX_UDP_PACKET_SIZE 548

struct ETHPACKET
{
	uint8_t packet[MAX_RX_BUFFER_SIZE];
	size_t size;
};

enum PROTO_INDEX {
	IP_PROTO=0,
	TCP_PROTO=1,
	DATA_BEGIN=2,
	MAX_PROTO
};
struct TCPPOOL_PACKET
{
	uint32_t head_size[MAX_PROTO];
	uint8_t *head_proto[MAX_PROTO];
	ip_header *ipheader;
	tcp_header *tcpheader;
	uint8_t packet[MAX_RX_BUFFER_SIZE];
	size_t size;
};

enum TCP_STATE
{
	CLOSED=0,
	LISTEN,
	SYN_SENT,
	SYN_ACK_SENT,
	SYN_RCVD,
	ESTABLISHED,
	/*FIN_WAIT1,
	FIN_WAIT2,
	CLOSE_WAIT,
	TIME_WAIT,
	LAST_ACK,
	CLOSING,*/
};

#define MAX_TCP_BUFFER 65536
#define MAX_TCP_MSS 1460

struct RX_BUFFER {
	uint8_t Buffer[MAX_TCP_MSS];
	uint64_t Seq;
	uint16_t size;
};

struct TX_BUFFER
{
	uint8_t Buffer[MAX_TCP_BUFFER];
	uint16_t size;
};

struct TCP_BLOCK
{
	uint32_t Ack;
	uint32_t Seq;
	uint32_t Mss;
	uint32_t RxWnd;
	uint32_t CurWnd;
	uint32_t ReSize;
};

enum TX_STATE {
	TX_DONE,
	TX_PROCESSING,
	TX_STOP,
};