<span style="font-weight: bold;">

tcpip stack 你懂的，非lwip实现，非lwip实现，非lwip实现。</span><div>
<span style="font-weight: bold;">Windows版本，应用层版，基于winpcap的版本，主要用作验证tcpip栈可以工作。

</span></div>
<div>
<span style="font-weight: bold;"><br></span></div>
<pre>
写这个的目的：
	完成力量训练挑战1：
		7天之内在自己最熟悉的平台实现一个tcpip栈，栈代码必须自己完成。
		以证明自己拥有力量。
</pre>
<div>
力量训练挑战系列：要求在自己最熟悉的平台上
</div>
<pre>
1)7天内实现一个Tcpip栈
	至少有udp栈,tcp栈，
	栈代码必须是自己完成的（底层硬件发包可以借助第三方）。

2)10天内实现一个现代编译器，
	编译器支持语言不现，
	生成机器码（最好是生成可执行文件——如果有链接器，最长时间可以加5天），
	至少支持平行编译，有速度优化和空间优化，
	代码必须是自己完成的，不可借助LLVM等编译器开发组件

3)40天内实现一个64位操作系统，
	引导支持UEFI+GPT,和传统bios+MBR
	要支持多进程，多线程
	要有图形，图形显示不能低于增强16MB色
	要有网络，tcpip栈
	要有文件系统支持，不能是linux的文件系统，
	代码中除了网络部分其他代码必须自己完成。
</pre>
<div>
当前进度：</div>
<div>
1)winpcap 引入头各种报错修复 完成</div>
<div>
2)common 我自己常用的头引入 完成</div>
<div>
3)packetio 发包和收报的winpcap封装 完成</div>
<div>
4)基本监听 完成</div>
<div>
5)MAC解析收发 完成</div>
<div>
6)ARP解析收发 完成</div>
<div>
7)IP解析收发 完成</div>
<div>
8)UDP解析收发 完成</div>
<div>
9)DHCP解析收发 dhcpclient完成</div>
<div>
10)IP层重发机制 完成</div>
<div>
11)Eth异步完成</div>
<div>
12)udp socket测试ok</div>
<div>
13)icmp ping完成，icmpsocket基本工作完成</div>
<div>
14)tcp基本完成</div>
<div>
FIX 列表：</div>
<div>
TCP解析收发</div>
<pre>
TCP压力测试，接收与发送逻辑
</pre>
<div>
日程表：</div>
<pre>
2018-04-22 开始工程，packetio,common头，winpcap报错处理
2018-04-23 完成eth arp ip三层
2018-04-24 完成udp icmp的代码，dhcpclient和ping测试ok
2018-04-26 完成TCP基本模型
2018-04-27 完成TCP 握手和挥手处理，接收数据功能
2018-04-28 完成TCP 发送数据功能，KeepAlive,RTTM重传,修复接收数据顺序Overflow问题,开始压力测试
2018-05-01 提交工程DivertTcpip 
</pre>
<div>
TODO表:
</div>
<pre>
1)dnsclient 待有时间
2)httpclient 待有时间
3)自写WFP驱动，从ipv4层作收发接口（单独工程 WfpTcpip完成后公开）
4)纯驱动版，（工程 KernelTcpip 开源待定）
</pre>
<div>
<br></div>
<div>
<br></div>
