#pragma once


class CArp :
	public TcpipStack
{
public:
	CArp();
	~CArp();
private:
	std::unordered_map<unsigned long,mac_store> m_mac_map;
	std::vector<sock_net_nic_info> _nic_info;
private:
	void init_win_nic_info();
	unsigned long arp_ip_from_ip(unsigned long ip);
	int arp_add_ip_mac(unsigned long ip, void *mac);
	bool ReplyArp(const uint8_t*packet,size_t packet_len);
	bool SendArpQuary(unsigned long dst_ip, unsigned long src_ip, void* src_mac);
	void init_arp_header(struct arp_header* arp_hdr, unsigned long dst_ip, void* dst_mac, unsigned long src_ip, void* src_mac);
public:
	void addMacIp(uint32_t ip, uint8_t*mac) {
		arp_add_ip_mac(ip, (void*)mac);
	}
	bool ProcessRx(const uint8_t *packet, size_t packet_len);
	bool TxProcess(const uint8_t *packet, size_t packet_len);
	bool ProcessRxEx(const uint32_t dstIp, const uint32_t srcIp, const uint8_t *packet, size_t packet_len)
	{
		return false;
	}
public:
	//��ѯЭ��
	const uint8_t* Protocol2Hardware(const uint8_t* protocolAddress);
};

extern CArp arp;